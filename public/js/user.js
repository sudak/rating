function show_message(data){
	$('#message').html(data.text);
	if (data.code == 1){
		$('#message').removeClass('error');
		$('#message').addClass('ok');
	};
	if (data.code == 2){
		$('#message').removeClass('ok');
		$('#message').addClass('error');
	};

};
function checkUsername(username){
	var url = '/form/';
	var request = new Object();
	request['method'] = 'checkUser';
	request['model'] = 'users';
	request['username'] = username;
	$.ajax({
		type: 'POST',
		url: url,
		data: request,
		success: function(response){
			show_message(response)
		},
		dataType: 'json'
	});
};
function checkEmail(username){
	var url = '/form/';
	var request = new Object();
	request['method'] = 'checkEmailAvailability';
	request['model'] = 'users';
	request['email'] = username;
	$.ajax({
		type: 'POST',
		url: url,
		data: request,
		success: function(response){
			show_message(response)
		},
		dataType: 'json'
	});

};