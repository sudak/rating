<?php /* Smarty version Smarty-3.1.8, created on 2012-08-08 14:35:06
         compiled from "Z:\home\dev.ss\www\application\views\admin\main.tpl" */ ?>
<?php /*%%SmartyHeaderCode:255825022bf6a5b60a7-02539134%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ca2d0dea6960dbacf91f8e992cf8a8a614f6a5bd' => 
    array (
      0 => 'Z:\\home\\dev.ss\\www\\application\\views\\admin\\main.tpl',
      1 => 1344453270,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '255825022bf6a5b60a7-02539134',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
    'description' => 0,
    'styles' => 0,
    'style' => 0,
    'scripts' => 0,
    'script' => 0,
    'content' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5022bf6a83d793_52288102',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5022bf6a83d793_52288102')) {function content_5022bf6a83d793_52288102($_smarty_tpl) {?><!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
	<meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['description']->value;?>
" />
<?php  $_smarty_tpl->tpl_vars["style"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["style"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['styles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["style"]->key => $_smarty_tpl->tpl_vars["style"]->value){
$_smarty_tpl->tpl_vars["style"]->_loop = true;
?>
	<link href="<?php echo URL::base();?>
public/css/<?php echo $_smarty_tpl->tpl_vars['style']->value;?>
.css" rel="stylesheet" type="text/css" />
<?php } ?>
<?php  $_smarty_tpl->tpl_vars["script"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["script"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['scripts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["script"]->key => $_smarty_tpl->tpl_vars["script"]->value){
$_smarty_tpl->tpl_vars["script"]->_loop = true;
?>
	<script type="text/javascript" src="/public/js/<?php echo $_smarty_tpl->tpl_vars['script']->value;?>
.js"></script>
<?php } ?>
</head>

<body>
<div class="layer">
	<div class="container">
		<div class="header"><h1>Admin pannel</h1></div>

		<div class="left">
			<h3>Navigation</h3>
			<br />
			<ul>
				<li class="pannel blue"><a href="<?php echo URL::site();?>
">Main</a></li>
				<li class="pannel blue"><a href="<?php echo URL::site('about');?>
">About</a></li>
				<li class="pannel blue"><a href="<?php echo URL::site('user/addnews');?>
">Add news</a></li>
				<li class="pannel blue"><a href="<?php echo URL::site('contacts');?>
">Contacts</a></li>
			</ul>
		</div>
		<div class="content"><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</div>

		<div class="clearing"></div>
	</div>
</div>
</body>

</html><?php }} ?>