<?php /* Smarty version Smarty-3.1.8, created on 2012-08-08 13:09:57
         compiled from "Z:\home\dev.ss\www\application\views\user\create.tpl" */ ?>
<?php /*%%SmartyHeaderCode:294605022ab752b8861-35451223%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1ca960e61f0d78c938a19637d9a3b4ac47c71ffa' => 
    array (
      0 => 'Z:\\home\\dev.ss\\www\\application\\views\\user\\create.tpl',
      1 => 1344405428,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '294605022ab752b8861-35451223',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'message' => 0,
    'username' => 0,
    'errors' => 0,
    'email' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5022ab75491c49_54326996',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5022ab75491c49_54326996')) {function content_5022ab75491c49_54326996($_smarty_tpl) {?><h2>Create a New User</h2>
<?php if ($_smarty_tpl->tpl_vars['message']->value){?>
<h3 class="message">
	<?php echo $_smarty_tpl->tpl_vars['message']->value;?>

</h3>
<?php }?>

<?php echo Form::open('user/create');?>

<div id='message'></div>
<?php echo Form::label('username','Username');?>

<?php echo Form::input('username',$_smarty_tpl->tpl_vars['username']->value);?>

<div class="error">
	<?php echo Arr::get($_smarty_tpl->tpl_vars['errors']->value,'username');?>

</div>

<?php echo Form::label('email','Email Address');?>

<?php echo Form::input('email',$_smarty_tpl->tpl_vars['email']->value);?>

<div class="error">
	<?php echo Arr::get($_smarty_tpl->tpl_vars['errors']->value,'email');?>

</div>

<?php echo Form::label('password','Password');?>

<?php echo Form::password('password');?>

<div class="error">
	<?php echo Arr::path($_smarty_tpl->tpl_vars['errors']->value,'_external.password');?>

</div>

<?php echo Form::label('password_confirm','Confirm Password');?>

<?php echo Form::password('password_confirm');?>

<div class="error">
	<?php echo Arr::path($_smarty_tpl->tpl_vars['errors']->value,'_external.password_confirm');?>

</div>

<?php echo Form::submit('create','Create User');?>

<?php echo Form::close();?>


<p>Or <?php echo HTML::anchor('user/login','login');?>
 if you have an account already.</p>
<script type="text/javascript">
	$(document).ready(function(){
		$('input[name="username"]').change(function(){
			checkUsername($(this).val());
		});
		$('input[name="email"]').change(function(){
			checkEmail($(this).val());
		});
	});
</script><?php }} ?>