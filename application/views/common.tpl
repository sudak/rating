<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>{$title}</title>
	<meta name="description" content="{$description}" />
	{foreach from=$styles item="style"}
		<link href="{URL::base()}public/css/{$style}.css" rel="stylesheet" type="text/css" />
	{/foreach}
	{foreach from=$scripts item="script"}
		<script type="text/javascript" src="/public/js/{$script}.js"></script>
	{/foreach}
</head>

<body>
<div class="layer">
	<div class="container">
		<div class="header"><h1>Logo</h1></div>

		<div class="left">
			<h3>Navigation</h3>
			<br />
			<ul>
				<li><a href="{URL::site()}">Main</a></li>
				<li><a href="{URL::site('about')}">About</a></li>
				<li><a href="{URL::site('contacts')}">Contacts</a></li>
			</ul>
		</div>
		<div class="content">{$content}</div>

		<div class="clearing"></div>
		<div class="footer">2012</div>
	</div>
</div>
</body>
</html>