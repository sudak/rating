<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Common extends Controller_Template {

	public $template = 'common';

	public function before()
	{
		parent::before();
		View::set_global('title', 'News Site');
		View::set_global('description', 'Best news Site');
//		View::set_global('username', HTML::chars(Arr::get($_POST, 'username')));
		$this->template->title = 'News Site';
		$this->template->description = 'Best news Site';
		$this->template->content = '';
		$this->template->styles = array('main');
		$this->template->scripts = array('lib/jquery-1.7.2');
	}

} // End Common