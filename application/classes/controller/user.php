<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Common {

	private $_model = null;

	public  function __construct(Request $request, Response $response) {

		parent::__construct($request, $response);

		$this -> _model =  Model::factory('user');

	}

	public function action_index() {

		$page = 0;

		if (isset($_GET['page'])) {
			$page = $_GET['page'];
		}

		if (isset($_GET['date'])) {
			$date = $_GET['date'];
		} else {
			$date = time();
		}

		if (isset($_GET['mode'])) {
			$mode = $_GET['mode'];
		} else {
			$mode = 'all';
		}

		$rows = $this -> _model -> getTop($page, $date, $mode);

		$paging = $this -> _getPaging($this -> _model -> getTopCount($date, $mode));

		$weeks = $this -> _getWeeks();

		$months = $this -> _getMonths();

		$content = View::factory('/user/common')
			->bind('rows', $rows)
			->bind('paging', $paging)
			->bind('current_page', $page)
			->bind('current_date', $date)
			->bind('months', $months)
			->bind('mode', $mode)
			->bind('weeks', $weeks);

		$this -> template -> content = $content;

	}

	public function action_adduser() {

		if (isset ($_POST['user'])) {

			$user = $_POST['user'];

			$this -> _model -> addUser($user);

		}

		$this -> action_index();

	}

	public function action_delluser() {

		if (isset ($_GET['user'])) {

			$user = $_GET['user'];

			$this -> _model -> dellUser($user);

		}

		$this -> action_index();

	}

	public function action_addscore()
	{

		if (isset ($_GET['user'])) {

			$user = $_GET['user'];

			print_r($user);

			$this -> _model -> addAction($user,time());

		}

		$this -> action_index();

	}
	/**
	 * Create paging
	 * @param $count int
	 * @param $count_on_page int
	 * @return array
	 */
	private function _getPaging($count, $count_on_page = 10) {

		$result = array();

		for ($i=0; $i*$count_on_page<$count; $i++) {

			$result[$i]['title'] = $i+1;

			$result[$i]['value'] = $i;

		}

		return $result;

	}

	private function _getWeeks() {

		$result = array();

		$array_weeks = $this -> _model -> getActiveRange();

		foreach($array_weeks as $week) {

			$result[$week]['title'] =
					date('l jS \of F Y ',$week)
					.' - '
					.date(' l jS \of F Y ',($week+(6*24*60*60)));

			$result[$week]['value'] = $week;

		}

		return $result;

	}

	private function _getMonths() {

		$result = array();

		$array_months = $this -> _model -> getActiveRange('monthSet');

		foreach($array_months as $month) {

			$result[$month]['title'] = date('F ',$month);

			$result[$month]['value'] = $month;

		}

		return $result;

	}

}