<?php defined('SYSPATH') or die('No direct script access.');
/**
 *
 * Модель для добавления/извлечения пользователей.
 * Бд имеет избыточность за счет предрассчитанных заначений(примерно 18%), зато увеличили быстродействие
 * @ToDo Добавить отлов ошибок
 * @ToDo Сделать ревью
 * @ToDo Повыноситьт константы
 *
*/
class Model_User extends Model
{

	private $_users_key = 'users';

	private $_allowed_key = array('day', 'weak', 'month');

	public $count_elem_on_page = 10;



	/**
	 *Генерирует массив ключей по днях которые входят в диапазон
	 * @param $start_date timestamp
	 * @param $end_date timestamp
	 * @return array
	 */
	private function _generateRange($start_date, $end_date){

		$key = $this -> _users_key;

		$result = array();

		$rediska = new Rediska();

		$set = $rediska -> getFromSortedSetByScore($key.':daySet',$start_date,$end_date);

		foreach ($set as $date){

			$result[] = "$key:ranking:day:$date";

		}

		return $result;
	}


	/**
	 * Получаем массив существующих ключей для недели, месяца, дня
	 * @param $mode string (weakSet|daySat|monthSet)
	 * @return array
	 */
	public function getActiveRange($mode = 'weakSet'){

		$key = $this -> _users_key;

		$set =  new Rediska_Key_SortedSet("$key:$mode") ;

		$result = $set->toArray();

		return $result;
	}
	/**
	 * Получаем выборку рейтинга между произвольными датами
	 * @ToDo Метод не используется, потому что тяжелый
	 * @ToDo Переделать с использованием предрасчитанных данных и дельты
	 * @return array
	 */
	public function getTopByRange($start_date, $end_date)
	{

		$range = $this -> _generateRange($start_date, $end_date);

		$key = $this -> _users_key;

		$time_start =microtime()+time();

		$sortedSet = new Rediska_Key_SortedSet("$key:ranking:day:$start_date");

		$sortedSet -> union($range,'range') ;

		$result = new Rediska_Key_SortedSet("range");

		$result = $result->toArray(true);

		$time = time()+microtime() - $time_start;

		return $result;

	}

	/**
	 * Получаем массив постраничный массив с рейтином
	 * @param $page int
	 * @param $date timecnamp
	 * @param $mode string (all|day|weak|month)
	 * @return array
	 */
	public function getTop($page=0, $date = null, $mode="all")
	{
		$key = $this -> _users_key.':ranking';

		$rediska = new Rediska();

		$key_timestamp = $this -> _getKeyDay($date);

		if (in_array($mode,$this -> _allowed_key) AND !empty($date)){

			$key = "$key:$mode:".$key_timestamp[$mode];

		}

		$result = $rediska->getSortedSet(
			$key,
			true,
			($page)* $this -> count_elem_on_page,
			(($page)* $this -> count_elem_on_page)+$this -> count_elem_on_page,
			true
		);

		foreach ($result as $elem => $user) {

			$result[$elem]['rank'] = $rediska->getRankFromSortedSet($key,$user['value'],true)+1;

		}

		return $result;

	}

	public function getTopCount($date = null, $mode="all") {

		$key = $this -> _users_key.':ranking';

		$rediska = new Rediska();

		$key_timestamp = $this -> _getKeyDay($date);

		if (in_array($mode,$this -> _allowed_key)){

			$key = "$key:$mode:".$key_timestamp[$mode];

		}

		$result = $rediska->getSortedSetLength ($key);


		return $result;

	}
	/**
	 * Get value by key
	 * @param $key string
	 * @return array
	 */
	public function getValueByKey($key) {

		$rediska = new Rediska();

		$result = $rediska -> get($key);

		return $result;

	}

	/**
	 * Increment Score
	 * @param $user string
	 * @return integer
	 */
	public function incUserScore($user) {

		$key = $this -> _users_key;

		$sortedSet = new Rediska_Key_SortedSet($key.':ranking');

		$result = $sortedSet -> incrementScore($user,1);

		return $result;

	}
	/**
	 * Увеличиваем рейтинг и обновляем предрасчитанные данные
	 * @param $user string
	 * @param $timestamp UNIX timestamp
	 */
	public function addAction($user,$timestamp) {

		$date = $this -> _getKeyDay($timestamp);

		$rediska = new Rediska();

		//Через трубу в два раза быстрее
		$rediska -> pipeline()
			-> incrementScoreInSortedSet($this -> _users_key.':ranking:day:'.$date['day'],$user,1)
			-> incrementScoreInSortedSet($this -> _users_key.':ranking:weak:'.$date['weak'],$user,1)
			-> incrementScoreInSortedSet($this -> _users_key.':ranking:month:'.$date['month'],$user,1)
			-> incrementScoreInSortedSet($this -> _users_key.':ranking',$user,1)
			-> addToSortedSet($this -> _users_key.":daySet",$date['day'],$date['day'])
			-> addToSortedSet($this -> _users_key.":weakSet",$date['weak'],$date['weak'])
			-> addToSortedSet($this -> _users_key.":monthSet",$date['month'],$date['month'])
			-> execute();

		return;

	}
	/**
	 * @Todo Сделать схему и обрабатывать удаление через нее
	 * Удаляем пользователя и все его следы во всех множествах
	 * @param $user string
	 */
	public function dellUser($user) {


		$rediska = new Rediska();

		foreach ($this -> getActiveRange('daySet') as $date){

			$rediska -> deleteFromSortedSet($this -> _users_key.':ranking:day:'.$date, $user);

		}

		foreach ($this -> getActiveRange('weakSet') as $date){

			$rediska -> deleteFromSortedSet($this -> _users_key.':ranking:weak:'.$date, $user);

		}

		foreach ($this -> getActiveRange('monthSet') as $date){

			$rediska -> deleteFromSortedSet($this -> _users_key.':ranking:month:'.$date, $user);

		}

		$rediska -> deleteFromSortedSet($this -> _users_key.':ranking', $user);

		return;

	}

private function _getKeyDay($timestamp){

	$date = getdate($timestamp);

	$key_timestamp['day'] = mktime(0,0,0,$date['mon'],$date['mday'],$date['year']);

	$key_timestamp['weak'] = mktime(0,0,0,$date['mon'],($date['mday']-$date['wday']+1),$date['year']);

	$key_timestamp['month'] = mktime(0,0,0,$date['mon'],1,$date['year']);

	return $key_timestamp;
}

	/**
	 * Set value by key
	 * @param $key string
	 * @return array
	 */
	public function setValueByKey($key, $value) {

		$record = new Rediska_Key($key);

		$result = $record -> setValue($value);

		return $result;

	}

	/**
	 * Add user
	 * @param $user string
	 * @return array
	 */
	public function addUser($user) {

		$users_set = new Rediska();

		$result = $users_set -> addToSortedSet($this -> _users_key.':ranking', $user, 0);

		return $result;

	}


	/**
	 *Временный метод для быстрого заполнения бд
	 * @ToDo Методу тут не месо
	 * расскоментировав строчки модем занчительно увеличит количество пользователей
	 */
	public function fillUser() {

		$names = 'Amado Litchford Bobbie Halm Adriana Stengel Nickolas Roepke Yetta Woodfin Georgiana Sine Dot Hodgin Norah Piekarski Ute Valliere Gustavo Turpen Margarite Fencl Hilma Hedge Kiesha Corona Terese Boice Arlette Striplin Kurtis Stubbe Luise Ochoa Damion Gaal Williams Albino Kimbra Pickle Catrina Leclaire Catheryn Evert Shizuko Berthold Carolina Duplessis Meredith Caraballo Dana Acker Junior Ferriera Derek Lamoureaux Vashti Sams Cheryl Tai Narcisa Hickman Sharri Ross Tamisha Class Marsha Thornton Jodie Berkowitz Corinne Ganz Carmelita Cygan Joelle Beaubien Pauletta Na Kirsten Lester Nickie Czech Willa Oconnell Lawrence Bransford Eleanor Andres Suzette Blackshire Sherri Guice Charita Coulombe Saturnina Novack Shea Eisenmenger';

		$arr_names = explode(' ', $names);

		foreach ($arr_names as $name){
//			for ($i=0; $i<100; $i++){

//				$name=$name.$i;

				$this -> addUser($name);

//			}

		}
//		print_r($i);
		return ;

	}


	/**
	 *Временный метод для быстрого заполнения бд
	 * @ToDo Методу тут не месо
	 */
	public function fillRanking() {

		foreach ($this->get_all() as $user){

			//for date

			for ($i=30; $i <=120;  $i++){

				//for action per day
				$count_action = rand(0,3);

				$day = mktime(0, 0, 0, 1, $i, 2011);

				for ($j=1; $j < $count_action; $j++){

					$this -> addAction($user['value'],$day);

				}

			}

		}

		return ;

	}



}